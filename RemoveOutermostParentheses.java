class Solution {
    public String removeOuterParentheses(String s) {
        StringBuilder result = new StringBuilder();
        int openCount = 0;
        int startIndex = 0;

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(') {
                openCount++;
            } else if (c == ')') {
                openCount--;
                if (openCount == 0) {
                    // Found a primitive substring
                    result.append(s.substring(startIndex + 1, i));
                    startIndex = i + 1; // Update startIndex for the next primitive substring
                }
            }
        }

        return result.toString();
    }
}
